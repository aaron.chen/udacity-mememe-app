//
//  TextFieldDelegate.swift
//  MemeMe App
//
//  Created by Aaron Chen on 2/20/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

class TextFieldDelegate: NSObject, UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //clear text
        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //dismiss keyboard
        textField.resignFirstResponder()
        
        return true
    }
}
