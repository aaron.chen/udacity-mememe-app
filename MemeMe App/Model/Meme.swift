//
//  Meme.swift
//  MemeMe App
//
//  Created by Aaron Chen on 2/22/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

struct Meme {
    var topText: String
    var bottomText: String
    var originalImage: UIImage?
    var memedImage: UIImage?
}
