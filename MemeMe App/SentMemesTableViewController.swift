//
//  SentMemesTableViewController.swift
//  MemeMe App
//
//  Created by Aaron Chen on 3/4/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SentMemesTableCell"

class SentMemesTableViewController: UITableViewController {
    
    var memes: [Meme]! {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            return appDelegate.memes
        }
        else {
            print("could not downcast appDelegate")
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addMeme))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showMemeDetails":
            let memeDetailsVC = segue.destination as? MemeDetailsViewController
            let memedImage = sender as? UIImage
            memeDetailsVC?.memedImage = memedImage
        default:
            print("no preparation for segue")
        }
    }
    
    @objc func addMeme() {
        performSegue(withIdentifier: "addMeme", sender: self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.memes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        let meme = self.memes[indexPath.row]
        cell.textLabel?.text = meme.topText + "..." + meme.bottomText
        cell.imageView?.image = meme.memedImage

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let meme = self.memes[indexPath.row]
        let memedImage = meme.memedImage
        
        performSegue(withIdentifier: "showMemeDetails", sender: memedImage)
    }

}
