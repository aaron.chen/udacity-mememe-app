//
//  SentMemesCollectionViewController.swift
//  MemeMe App
//
//  Created by Aaron Chen on 3/4/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SentMemesCollectionCell"

class SentMemesCollectionViewController: UICollectionViewController {

    var memes: [Meme]! {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            return appDelegate.memes
        }
        else {
            print("could not downcast appDelegate")
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addMeme))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionView.reloadData()
    }

    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showMemeDetails":
            let memeDetailsVC = segue.destination as? MemeDetailsViewController
            let memedImage = sender as? UIImage
            memeDetailsVC?.memedImage = memedImage
        default:
            print("no preparation for segue")
        }
    }
    
    @objc func addMeme() {
        performSegue(withIdentifier: "addMeme", sender: self)
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.memes.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SentMemesCollectionViewCell
    
        let meme = self.memes[indexPath.row]
        cell.imageView?.image = meme.memedImage
        
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let meme = self.memes[indexPath.row]
        let memedImage = meme.memedImage
        
        performSegue(withIdentifier: "showMemeDetails", sender: memedImage)
    }

}
