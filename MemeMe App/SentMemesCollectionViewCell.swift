//
//  SentMemesCollectionViewCell.swift
//  MemeMe App
//
//  Created by Aaron Chen on 3/6/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

class SentMemesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
