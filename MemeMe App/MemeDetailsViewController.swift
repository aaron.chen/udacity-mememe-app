//
//  MemeDetailsViewController.swift
//  MemeMe App
//
//  Created by Aaron Chen on 3/6/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

class MemeDetailsViewController: UIViewController {
    
    var memedImage: UIImage!

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        imageView.image = memedImage
    }

}
