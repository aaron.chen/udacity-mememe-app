//
//  MemeEditorViewController.swift
//  MemeMe App
//
//  Created by Aaron Chen on 2/19/19.
//  Copyright © 2019 Udacity. All rights reserved.
//

import UIKit

class MemeEditorViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var topTextField: UITextField!
    @IBOutlet weak var bottomTextField: UITextField!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var navbar: UINavigationBar!
    
    let topTextFieldDelegate = TextFieldDelegate()
    let bottomTextFieldDelegate = TextFieldDelegate()
    
    // MARK: collections
    enum ImagePickerType: Int {
        case camera = 0,
        album
    }
    
    enum TextFieldType: Int {
        case top = 0,
        bottom
    }
    
    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //add subscriptions
        subscribeAll()
        //add delegates
        setDelegates()
        
        //disable camera button if not available
        cameraButton.isEnabled = UIImagePickerController.isSourceTypeAvailable(.camera)
        //set text attributes
        setDefaultTextFieldAttributes(topTextField)
        setDefaultTextFieldAttributes(bottomTextField)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //remove subscriptions
        unsubscribeAll()
    }
    
    // MARK: Action methods
    @IBAction func pickAnImage(_ sender: UIBarButtonItem) {
        
        //present correct image picker based on sending toolbar item
        if let pickerType = ImagePickerType(rawValue: sender.tag) {
            let pickerController = UIImagePickerController()
            pickerController.delegate = self
            // present image picker over current screen so as to not screw up nav bar
            pickerController.modalPresentationStyle = .overCurrentContext
            
            switch(pickerType) {
            case .camera:
                pickerController.sourceType = .camera
            case .album:
                pickerController.sourceType = .photoLibrary
            }
            
            present(pickerController, animated: true, completion: nil)
        }
        else {
            print("Bad picker type")
        }
    }
    
    @IBAction func shareImage(_ sender: Any) {
        //create a memeified image, then share it
        if let memedImage = generateMemedImage() {
            let activityViewController = UIActivityViewController(activityItems: [memedImage], applicationActivities: nil)
            
            //share the image if user selects a service
            activityViewController.completionWithItemsHandler = {
                (activityType, completed, returnedItem, activityError) in
                
                if completed && activityError == nil {
                    self.save(memedImage)
                    
                    self.dismiss(animated: true, completion: nil)
                }
                else if (activityError != nil) {
                    print(activityError!)
                }
            }
            
            present(activityViewController, animated: true, completion: nil)
        } else {
            print("could not create memedImage")
        }
    }
    
    @IBAction func clearMeme(_ sender: Any) {
        resetTextField(topTextField)
        resetTextField(bottomTextField)
        imageView.image = nil
        
        dismiss(animated: true, completion: nil)
    }

    // MARK: Helper methods
    
    func setDelegates() {
        self.topTextField.delegate = topTextFieldDelegate
        self.bottomTextField.delegate = bottomTextFieldDelegate
    }
    
    func setDefaultTextFieldAttributes(_ textField: UITextField) {
        setMemeTextAttributes(textField) //make it look meme-y
        textField.textAlignment = .center //center text
    }
    
    func resetTextField(_ textField: UITextField) {
        if let fieldType = TextFieldType(rawValue: textField.tag) {
            switch fieldType {
            case .top:
                textField.text = "TOP"
            case .bottom:
                textField.text = "BOTTOM"
            }
        }
        else {
            print("Bad text field type")
        }
    }
    
    func setMemeTextAttributes(_ textField: UITextField) {
        let memeTextAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.strokeColor: UIColor.black,
            NSAttributedString.Key.strokeWidth: -5,
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-CondensedBlack", size: 40)!
        ]
        textField.defaultTextAttributes = memeTextAttributes
    }
    
    func generateMemedImage() -> UIImage? {
        var memedImage: UIImage?
        
        //hide toolbar / navbar
        toolbar.isHidden = true
        navbar.isHidden = true
        
        //Render view to image
        UIGraphicsBeginImageContext(self.view.frame.size)
        view.drawHierarchy(in: self.view.frame, afterScreenUpdates: true)
        if let renderedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            memedImage = renderedImage
        } else {
            memedImage = nil
        }
        UIGraphicsEndImageContext()
        
        //show toolbar / navbar
        toolbar.isHidden = false
        navbar.isHidden = false
        
        return memedImage
    }
    
    func getKeyboardHeight(_ notification: Notification) -> CGFloat? {
        if let keyboardInfo = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardSize = keyboardInfo.cgRectValue.height
            return keyboardSize
        } else {
            return nil
        }
    }
    
    @objc func save(_ memedImage: UIImage) {
        // Create the meme
        let meme = Meme(topText: topTextField.text ?? "",
                        bottomText: bottomTextField.text ?? "",
                        originalImage: imageView.image,
                        memedImage: memedImage)
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            // Add meme to memes array
            appDelegate.memes.append(meme)
        } else {
            print("could not cast shared delegate to AppDelegate")
        }
    }
    
    // MARK: Image Picker Delegate methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
        } else {
            print("no image returned?")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Subscription methods
    func subscribeAll() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unsubscribeAll() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        //move frame up if bottom field is being edited
        if bottomTextField.isEditing {
            if let keyboardHeight = getKeyboardHeight(notification) {
                view.frame.origin.y = -keyboardHeight
            } else {
                print("unable to get keyboard height")
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        // move frame back to origin when keyboard is hidden
        view.frame.origin.y = 0
    }
}

